/**
 * Created by front on 2018-03-05.
 */



define(['exports','jquery'], function (exports, $) {
    'use strict';

    /*
     Home Slide
     1. currentScrollHeight 800px 일때 window prevent scroll event
     2. currentSlide 가 1번 이면 mouse wheel 이벤트 시  left - window.Width
     3. currentSlide 1번 이면 mouse wheel 이벤트 stop, scroll 가능
     4. currentSlide 3번 이면 mouse wheel 이벤트 stop, scroll 가능

     @param
     element : slide 영역 element
     userOption:
     {
     slidesContainer: element,
     slides: '.slide',
     animateDuration: '500',
     animationEasing: 'swing',
     mouseWheelArea: element,
     width: '100%'
     }
     callback: callback 함수
     */

    var VivarHomeSlider = function (element, userOption, callback) {
        var VivarHomeSlider = function (element, userOption, callback) {

            if (element === undefined) throw new Error("Element 인자가 없습니다.");

            var _ = this,
                eventHandler,
                isDisabled = false,
                options = null;

            if (typeof callback === 'function') {
                eventHandler = callback;
            }

            userOption = userOption || {};

            options = $.extend({
                slidesContainer: element,
                slides: '.slide',
                animateDuration: '500',
                animationEasing: 'swing',
                mouseWheelArea: element,
                width: '100%'
            }, userOption);



            if ($(options.slides).length === 0) return;

            _.currentSlide = 1;

            var init = function () {
                _.currentSlide = 1;
                _.slideArray = $(options.slidesContainer).find(options.slides);
                _.slideHeight = $(_.slideArray[0]).height() + 50; //TODO slide들 높이가 안맞아서 임시로
                _.totalSlide = _.slideArray.length;

                $(options.slidesContainer).css({
                    position: 'relative',
                    overflow: 'hidden',
                    height: _.slideHeight,
                    width: options.width
                }).wrapInner($("<div class='slide-container clearfix'></div>").css({
                    position: 'relative',
                    width: 100 * _.totalSlide + '%'
                }));

                $(_.slideArray).each(function () {
                    $(this).css({ float: 'left', width: (100 / _.totalSlide) + '%' })
                });

                _.eventAreaHeight = $(options.mouseWheelArea).height();
                _.eventAreaOffsetTop = $(options.mouseWheelArea).offset().top;
                _.eventAreaOffsetBottom = _.eventAreaOffsetTop + _.eventAreaHeight;


                $(window).resize(resizeCallBack); // window resize 이벤트 등록
                $(options.mouseWheelArea).on('mousewheel DOMMouseScroll', determineDirection); // mouse scroll 이벤트 등록
            }

            init(); // 초기 설정 함수 실행

            function onChangeSlide(oldVal) {
                if (eventHandler !== undefined) {
                    eventHandler.apply(this, [oldVal, _.currentSlide]);
                }
                setTimeout(function () { //TODO 마우스 휠 돌린만큼 실행 되기 때문에 한번만 실행하도록 처리해야한다. 지금은 임시 방편
                    isDisabled = false;
                }, 300)
            }

            function determineDirection(event) {
                event.preventDefault();

                if (isDisabled === false) {
                    if (event.originalEvent.wheelDelta === -150 || event.originalEvent.wheelDelta === -120) {
                        if (_.currentSlide === _.totalSlide) {
                            isDisabled = true; //TODO 마우스 휠 돌린만큼 실행 되기 때문에 한번만 실행하도록 처리해야한다. 지금은 임시 방편
                            onChangeSlide(_.currentSlide); // event callback 함수 실행
                        } else {
                            _.nextSlide();
                        }
                    } else {
                        if (_.currentSlide === 1) {
                            isDisabled = true; //TODO 마우스 휠 돌린만큼 실행 되기 때문에 한번만 실행하도록 처리해야한다. 지금은 임시 방편
                            onChangeSlide(_.currentSlide); // event callback 함수 실행
                        } else {
                            _.preSlide();
                        }
                    }
                }
            }
            function resizeCallBack() {
                _.eventAreaHeight = $(options.mouseWheelArea).height();
                _.eventAreaOffsetTop = $(options.mouseWheelArea).offset().top;
                _.eventAreaOffsetBottom = _.eventAreaOffsetTop + _.eventAreaHeight;
            }
            _.nextSlide = function () {
                isDisabled = true;
                var oldVal = _.currentSlide;
                _.currentSlide += 1;

                onChangeSlide(oldVal); // event callback 함수 실행

                $('.slide-container').animate({ marginLeft: '-' + (100 * (_.currentSlide - 1)) + '%' }, options.animateDuration, options.animationEasing, function () {
                    isDisabled = false;
                });
            }
            _.preSlide = function () {
                isDisabled = true;
                var oldVal = _.currentSlide;
                _.currentSlide -= 1;

                onChangeSlide(oldVal); // event callback 함수 실행

                $('.slide-container').animate({ marginLeft: '-' + (100 * (_.currentSlide - 1)) + '%' }, options.animateDuration, options.animationEasing, function () {
                    isDisabled = false;
                });
            }
        }

        return new VivarHomeSlider(element, userOption, callback);

    };

    return VivarHomeSlider;

});
////# sourceMappingURL=common.js.map
