/**
 * Created by lg on 2017-05-10.
 */
const getMiliToTime = (seconds) => {
  var pad = function(x) { return (x < 10) ? "0"+x : x; }
  let h = parseInt(Math.floor(seconds / (60*60)) ) <= 0 ? '': pad(parseInt(Math.floor(seconds / (60*60)) )) + 'h ';
  let m = parseInt(Math.floor((seconds % (60*60))/ 60)) <= 0 ? '': pad(Math.floor((seconds % (60*60))/ 60)) + 'm ';
  let s = pad((seconds % (60*60)) % 60) + 's';

  return h + m + s;
};

export default getMiliToTime;
