const sendPushMessage = (member) => {
    let token = localStorage.getItem('pushToken');

    const message = {
        "to": token,
        "priority": 'high',
        "notification": {
            "title": "MAXWORK REMOTE",
            "body": `${member.name}님으로부터 원격지원을 요청 받았습니다`,
            "icon": "/static/images/logo-remote.png",
            "tag": "simple-push-demo-notification-tag",
            "data": {
                "url": "https://remote.maxwork.maxst.com/#/calling"
            },
            "sound": '/static/MAXWORK_Remote_ringtone.mp3',
            "click_action": "/#/calling",
        },
    };
    fetch('https://fcm.googleapis.com/fcm/send', {
        method: 'POST',
        body: JSON.stringify(message),
        headers: new Headers({
            'Content-type': 'application/json',
            'Authorization': 'key=AAAAgvP3UJY:APA91bExXgyccMXS5xExt4K4liuRSOI1hbIJG2v9liMRc79FeJfFpYmBrNxkZnAb8pYrWh3AvpwqCmowasU6X7WIAnGkVv5VrA-oACq0-Wr-PLw7Qtoj5vHgC03F7w8ADwR4LcQ50JMX'
        })
    }).then(response => {
        if (response.status < 200 || response.status >= 400) {
            throw 'Error subscribing to topic: ' + response.status + ' - ' + response.text();
        }
    }).catch(e => {
        console.log(e)
    })
};

export default sendPushMessage;