

define(['exports', 'jquery'], function (exports, $) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.imageSize = exports.viewLocaleChangeValue = exports.MSG = exports.getCookie = exports.deleteCookie = exports.setCookie = undefined;

    // 쿠키 생성

    var setCookie = function setCookie(cName, cValue, cDay) {
        var expire = new Date();
        var cookieName = cName;
        expire.setDate(expire.getDate() + cDay);
        var cookies = cookieName + '=' + escape(cValue) + '; path=/ '; // 한글 깨짐을 막기위해 escape(cValue)를 합니다.
        if (typeof cDay !== 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
        document.cookie = cookies;
    };

    var deleteCookie = function deleteCookie(cookieName, locale) {
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() - 1);
        document.cookie = cookieName + '=' + '; expires=' + expireDate.toGMTString() + '; path=/';
        setCookie('mylocale', locale, 100);
        window.location.reload(true);
    };

    // 쿠키 가져오기
    var getCookie = function getCookie(cName) {
        var cookieName = cName + '=';
        var cookieData = document.cookie;
        var start = cookieData.indexOf(cName);
        var cValue = '';
        if (start !== -1) {
            start += cookieName.length;
            var end = cookieData.indexOf(';', start);
            if (end === -1) end = cookieData.length;
            cValue = cookieData.substring(start, end);
        }
        return unescape(cValue);
    };

    // Get cookie function
    var getPopupCookie = function getPopupCookie(name) {
        var cookieName = name + "=",
            lastChrCookie = 0,
            x = 0;
        while (x <= document.cookie.length) {
            var y = (x + cookieName.length);
            if (document.cookie.substring(x, y) == cookieName) {
                if ((lastChrCookie = document.cookie.indexOf(";", y)) == -1)
                    lastChrCookie = document.cookie.length;
                return decodeURI(document.cookie.substring(y, lastChrCookie));
            }
            x = document.cookie.indexOf(" ", x) + 1;
            if (x == 0)
                break;
        }
        return "";
    };

    /* setCookie function */
    var setPopupCookie = function setPopupCookie(expire) {

        document.cookie = "popup" + new Date().getDate() + "=" + encodeURI("end") + "; expires=" + expire + "; path=/;";
    }
    // 에러 메세지

    var numberWithCommas = function numberWithCommas(number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    // 이미지 사이즈 처리
    var imageSize = function imageSize() {
        var $imageGroup = $('.group-thumbnail img');
        $imageGroup.each(function () {
            if ($(this).width() > $(this).height()) {
                $(this).addClass('width-full');
            } else {
                $(this).addClass('height-full');
            }
            $(this).fadeIn();
        });
    };

    // 엑셀 Export 

    var tableToExcel = function tableToExcel(table, name, title) {

        var uri = 'data:application/vnd.ms-excel;base64,',
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml>' +
                '<x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets>' +
                '</x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><h3>{title}</h3><table>{table}</table></body></html>',
            base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                });
            };
        if (!table.nodeType) table = document.getElementById(table);
        var ctx = {
            worksheet: name || 'Worksheet',
            table: table.innerHTML,
            title: title
        };
        //          var blob = new Blob([format(template, ctx)]);
        //          var blobURL = uri + window.URL.createObjectURL(blob);
        var blobURL = uri + base64([format(template, ctx)]);
        return blobURL;

    }

    // 두 날짜 차이 계산

    var dateDiff = function dateDiff(date1, date2) {
        var diffDate_1 = new Date(date1);
        var diffDate_2 = new Date(date2);

        var diff = Math.abs(diffDate_2.getTime() - diffDate_1.getTime());
        diff = Math.ceil(diff / (1000 * 3600 * 24)); // 일 로 반환

        return diff;
    }

    // Time Format

    var timeFormat = function timeFormat(time, format) {

        var hh = Math.floor(time / 3600);
        var mm = Math.floor((time % 3600) / 60);
        var ss = (time % 3600) % 60;

        return format.replace(/(hh|mm|ss)/gi, function ($1) {
            switch ($1) {
                case 'hh':
                    hh = (hh < 10) ? '0' + hh.toString() : hh.toString();
                    return hh;
                case 'mm':
                    mm = (mm < 10) ? '0' + mm.toString() : mm.toString();
                    return mm;
                case 'ss':
                    ss = (ss < 10) ? '0' + ss.toString() : ss.toString();
                    return ss;
            }
        });

    }

    //get Url Parameter
    var getUrlParameter = function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

// remove Url Parameter
    var removeParameter = function removeParams(sParam) {
        var url = window.location.href.split('?')[0] + '?';
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] != sParam) {
                url = url + sParameterName[0] + '=' + sParameterName[1] + '&'
            }
        }
        return url.substring(0, url.length - 1);
    }


    var deepMerge = function deepMerge(target, source) {
        // Merge a `source` object to a `target` recursively
        // Iterate through `source` properties and if an `Object` set property to merge of `target` and `source` properties
        for (let key of Object.keys(source)) {
            if (source[key] instanceof Object) Object.assign(source[key], deepMerge(target[key], source[key]))
        }

        // Join `target` and modified `source`
        Object.assign(target || {}, source)
        return target
    }

    const getGuid = () => {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };

    exports.setCookie = setCookie;
    exports.deleteCookie = deleteCookie;
    exports.getCookie = getCookie;
    exports.getPopupCookie = getPopupCookie;
    exports.setPopupCookie = setPopupCookie;
    exports.numberWithCommas = numberWithCommas;
    exports.imageSize = imageSize;
    exports.tableToExcel = tableToExcel;
    exports.dateDiff = dateDiff;
    exports.timeFormat = timeFormat;
    exports.getUrlParameter = getUrlParameter;
    exports.removeParameter = removeParameter;
    exports.deepMerge = deepMerge;
    exports.getGuid = getGuid;

    return exports;

});
////# sourceMappingURL=common.js.map