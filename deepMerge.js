/**
 * Created by lg on 2017-05-10.
 */
const deepMerge = function deepMerge(target, source) {
  // Merge a `source` object to a `target` recursively
  // Iterate through `source` properties and if an `Object` set property to merge of `target` and `source` properties
  for (let key of Object.keys(source)) {
    if (source[key] instanceof Object) Object.assign(source[key], deepMerge(target[key], source[key]))
  }

  // Join `target` and modified `source`
  Object.assign(target || {}, source)
  return target
}

export default deepMerge;
