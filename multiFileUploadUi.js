/**
 * Created by front on 2018-03-05.
 */


var MultiFileUpload = function () {
    var MultiFileUpload = function () {
        var _ = this, filePicker, fileContainer, multiple, isCanCancel = true, options = null;

        _.init = function (element, userOption) {

            userOption = userOption || {};

            options = $.extend({
                maxFileSize: 3 //MB 단위
            }, userOption);

            filePicker = $('input[type=file]')[0];
            fileContainer = element;
            _.fileInfo = [];
            _.fileUploadedCount = 0;
            _.fileUploadedFailCount = 0;
            _.fileTotalCount = 0;
            _.fileCanUploadCount = 0;
            _.fileSizeTotal = 0;
            _.invalidFileTotalCount = 0;
            _.fileTotalSize = 0;
            _.invalidMsg = 'invalid filed';
            _.invalidFileList = [];

            var html = ''
            html += "<div class='ui-upload-wrap'>";
            html += "<div class='ui-upload-container' id='ui-upload-container'></div><hr/>";
            html += "<div class='ui-upload-summary'><span class='ui-upload-summary__total' id='ui-upload-summary__total'>0 of 0 uploaded</span>";
            html += "<div class='ui-upload-summary__progressbar-block'><div class='ui-upload-summary__progressbar' id='ui-upload-progressbar'></div></div>";
            html += "<div class='ui-upload-summary__info'></div>"
            html += "<div class='ui-upload-summary__desc' id='ui-upload-summary__desc'></div>";
            html += "</div>";
            $(html).css({
                position: 'relative',
            }).appendTo(fileContainer);

            $(filePicker).change(_.fileBindChange);
            $(fileContainer).on('click', '#ui-upload__cancel', _.removeSingleUpload);

        }

        _.fileBindChange = function (event) {
            _.clearAll();

            var files = null, filePath;

            if (filePicker.files) {
                files = filePicker.files;
                multiple = true;
            }
            _.onFileSelected(event, {
                files: files,
                multiple: multiple
            });
        }

        _.onFileSelected = function (event, data) {
            var files;
            if (data && data.files && data.files.length >= 1) {
                files = data.files;

                if (multiple === true) {
                    _.uploadMultiple(files);
                }
            }
        }
        _.onUploadComplete = function () {
            $('.ui-upload-summary__progressbar').css('width', '100%');
        }
        _.uploadMultiple = function (files) {
            for (var i = 0; i < files.length; i++) {
                if (_.validateFile(files[i], i)) {
                    _.fileTotalSize += files[i].size;
                    _.fileTotalCount++;
                    _.setFileInfo(files[i], i);
                }
                _.createDisplayUpload(files[i], i);
            }
        }

        _.validateFile = function (file, fileId) {
            var type = file.type;
            var size = file.size;
            var fileName = file.name.slice(0, file.name.lastIndexOf('.'));
            if (file != null) {
                if (!/(image\/gif)|(image\/png)|(image\/jpeg)/gi.test(type)) {
                    _.setFileInfo(file, fileId, { errorMsg: 'abnormal file', invalid: false })
                    return false
                }
                if (size > (parseInt(options.maxFileSize) * 1024 * 1024)) {
                    _.setFileInfo(file, fileId, { errorMsg: 'overceed file size', invalid: false });
                    return false;
                }
                if (!/^([a-zA-Z0-9_]+)$/gi.test(fileName)) {
                    _.setFileInfo(file, fileId, { errorMsg: 'invalid name', invalid: false });
                    return false
                }
            }
            return true;
        }
        _.disableButton = function (isDisable) {
            isDisable || false;
            if (isDisable) {
                $(filePicker).attr({ 'disabled': 'disabled' });
                isCanCancel = false;
            } else {
                $(filePicker).removeAttr('disabled');
                isCanCancel = true;
            }
        }

        _.createDisplayUpload = function (file, fileId) {
            var html = '';
            var fileSize = Number(_.fileInfo[fileId].fileSize / 1024).toFixed(1);
            html += "<div class='ui-upload-block' id='ui-upload-" + fileId + "'>";
            html += "<span class='ui-upload__fileName'><i class='fa fa-image'></i>" + _.fileInfo[fileId].fileName + "</span>";
            html += "<span class='ui-upload__error'>" + _.fileInfo[fileId].errorMsg + "</span>";
            html += "<span class='ui-upload__fileSize'>" + fileSize + "KB </span>";
            html += "<span class='ui-upload__status'><i class='fa fa-check'></i></span>";
            html += "<span class='ui-upload__cancel' id='ui-upload__cancel' data-id='" + fileId + "'><i class='fa fa-times'></i></span>";
            html += "</div>";

            $(html).appendTo('#ui-upload-container');

            _.displayFileTotalCount();

        }
        _.displayFileTotalCount = function () {
            var totalText = '0 of ' + _.fileTotalCount + ' uploaded';
            $('.ui-upload-summary__total').text(totalText);
        }
        _.getFileInfo = function () {
            return _.fileInfo;
        }
        _.setFileInfo = function (file, fileId, extra) {
            var fileName = file.name.slice(0, file.name.lastIndexOf('.'))
                , fileFullName = file.name
                , fileSize = file.size
                , fileId = fileId
                , key = randomString(30)
                , errorMsg = ''
                , status = 'ready'
                , invalid = true;

            _.fileInfo[fileId] = {
                key: key,
                fileId: fileId,
                fileName: fileName,
                fileFullName: fileFullName,
                fileSize: fileSize,
                errorMsg: errorMsg,
                status: status,
                data: file,
                base64Data: '',
                invalid: invalid
            }

            $.extend(_.fileInfo[fileId], extra);
            _.setBase64(file, fileId);
        }
        _.setStatus = function (fileId, extra) {
            if (typeof extra === 'string') {
                _.fileInfo[fileId].status = extra;
            }
            else if (typeof extra === 'object') {
                extra = extra || {};
                $.extend(_.fileInfo[fileId], extra);
            }

            if (_.fileInfo[fileId].status === 'fail') {
                _.fileInfo[fileId].valid = false;
                _.fileUploadedFailCount++;
                $('.ui-upload-summary__info').html('Failed: ' + _.fileUploadedFailCount);
                $('#ui-upload-' + fileId).find('.ui-upload__error').html(_.fileInfo[fileId].errorMsg);
            }

            if (_.fileInfo[fileId].status === 'complete') {
                _.fileUploadedCount++;
                var range = 100 / Number(_.fileTotalCount);
                $('#ui-upload-' + fileId).find('.fa-check').css({ 'color': '#00c3b3' });
                $('.ui-upload-summary__total').html(_.fileUploadedCount + ' of ' + _.fileTotalCount + ' uploaded');
                $('.ui-upload-summary__progressbar').css('width', range * _.fileUploadedCount + '%');

            }
        }
        _.removeSingleUpload = function () {
            if (isCanCancel) {
                var fileId = $(this).data('id');
                _.fileInfo.splice(fileId, 1);

                _.fileTotalCount = _.fileInfo.length;
                $('#ui-upload-' + fileId).hide().remove();
                _.displayFileTotalCount();
            }
            return false;
        }
        _.clearAll = function () {
            if (_.fileInfo.length === 0) {
                return;
            }
            _.fileInfo = [];
            _.fileUploadedCount = 0;
            _.fileUploadedFailCount = 0;
            _.fileTotalCount = 0;
            _.fileSizeTotal = 0;

            _.displayFileTotalCount();
            $('#ui-upload-container').empty();
            $('.ui-upload-summary__info').html('');
            $('.ui-upload-summary__desc').html('');
            $('.ui-upload-summary__progressbar').width(0);
        }
        _.setBase64 = function (file, fileId) {
            var reader = new FileReader();
            var result = '';
            reader.readAsDataURL(file);
            reader.onload = function () {
                result = reader.result;
                _.fileInfo[fileId].base64Data = result.replace(/^data:([a-zA-Z/]*);base64,/g, '');
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        }
        _.displaySummary = function (msg) {
            var totalFailCount = _.fileTotalCount - _.fileUploadedCount;
            $('.ui-upload-summary__info').html('Fail: ' + totalFailCount);
            $('.ui-upload-summary__desc').html(msg);
        }

        function randomString(stringLength) {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz",
                randomstring = "", i, rnum;
            for (i = 0; i < stringLength; i++) {
                rnum = Math.floor(Math.random() * chars.length);
                randomstring += chars.substring(rnum, rnum + 1);
            }
            return randomstring;
        }

    }
    return new MultiFileUpload();
}();
////# sourceMappingURL=common.js.map
