﻿/*
 * Depengency:
 * jQuery,
 * jQuery Mouse Wheel  https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.js
 * TweenMax https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js
 * ScrollToPlugin https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/plugins/ScrollToPlugin.min.js
 *
 * @param {string} element
 * @param  { Object } options {
 *  scrollTime
 *  scrollDistance
 * }
 * */

const SmoothScrollWithTweenMax = ( ($, TweenMax) => {

    const $window = $(window) //Window object

    let _elements = [];
    let _options = {};

    const evenCallback = (event) => {
        event.preventDefault();
        event.stopPropagation();

        let delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3
            ,scrollTop = $window.scrollTop()
            ,finalScroll = scrollTop - parseInt(delta * _options.scrollDistance);

        console.log(scrollTop);
        console.log(finalScroll);

        TweenMax.to($window, _options.scrollTime, {
            scrollTo : { y: finalScroll, autoKill:true },
            ease: Power1.easeOut,	//For more easing functions see https://api.greensock.com/js/com/greensock/easing/package-detail.html
            autoKill: true,
            overwrite: 5
        });
    };
    const setEventHandler = (elements) => {
        $.each(elements, function () {
            $(this).on("mousewheel DOMMouseScroll", evenCallback.bind(this));
        });
    };
    const initFunc = (elements, userOption) => {
        _elements = Array.isArray(elements) ? elements : elements.split(',');
        _options = $.extend({
            scrollTime : 1.2	//Scroll time
            ,scrollDistance : 170		//Distance. Use smaller value for shorter scroll and greater value for longer scroll
        }, userOption);
        setEventHandler(_elements);

        $window.resize(initFunc)
    };

    return {
        init: initFunc
    };
})(jQuery, TweenMax);

