﻿const ScrollMagicFactory = ( () => {
    var ScrollMagicFactory = function ScrollMagicFactory() {
        this.controller = new ScrollMagic.Controller();
        this.scenes = [];
    };

    var _scene = void 0;
    ScrollMagicFactory.prototype.createScrollAnimation = function (element, options) {
        var indicator = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];

        if (typeof element !== 'string') return new Error('no element parameter!!!');

        _scene = new ScrollMagic.Scene(Object.assign({}, { triggerElement: element }, options.scene));

        _scene.setTween(options.tween);
        _scene.addIndicators(indicator);
        _scene.addTo(this.controller);

        this.scenes.push(_scene);

        return this;
    };

    return new ScrollMagicFactory();
})();