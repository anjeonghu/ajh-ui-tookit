
import RecordRTC from 'recordrtc'

const recorderUtill = (() => {
    function RecorderUtill () {
        this.rtcRecorder = null;
        this.mimeType = 'video/webm;codecs=h264';
        this.option =  {
            type: 'video',
            mimeType: this.mimeType,
            timeslice: 1000,
        };
        this.duration = 0;
        this.blobs = [];
        this.blobType = null;
        this.files = [];
        this.videoFile = null;
        this.fileName = null;
        this.stream = new MediaStream();
        this.audioDestination = null;
        this.onerror = () => {};
    }
    RecorderUtill.prototype.init = function () {
        this.rtcRecorder = null;
        this.mimeType = 'video/webm;codecs=h264';
        this.option =  {
            type: 'video',
            mimeType: this.mimeType,
            timeslice: 1000,
        };
        this.blobs = [];
        this.duration = 0;
        if (this.stream && this.stream.getTracks()) {
            let tracks = this.stream.getTracks();
            for (let i = 0; i < tracks.length; i++) {
                tracks[i].stop();
            }
        }
        this.videoFile = null;
        this.fileName = null;
        this.stream = new MediaStream();
        this.audioDestination = null;
        this.onerror = () => {};
    };
    RecorderUtill.prototype.createRecorder = function (stream, option = null) {
        if(this.stream && stream) {
            stream.getVideoTracks().forEach((t) => {
                this.stream.addTrack(t);
            })
        }
        let _self = this;

        let options = option || this.option;
        let recorder = RecordRTC(this.stream, {
            type: options.video,
            mimeType: options.mimeType,
            timeSlice: options.timeslice, // pass this parameter
            ondataavailable: function(blob) {
                _self.blobs.push(blob)
            },
            onTimeStamp: function(timestamp, timestamps) {
                let duration = (new Date().getTime() - timestamps[0]) / 1000;
                console.log('duration --- ', duration)
                _self.duration = duration;
                if(duration < 0) return;
            }
        });
        let internalRecorder = recorder.getInternalRecorder();
        if(internalRecorder) {
            internalRecorder.onerror = function(error) {
                if (!error) {
                    return;
                }

                if (!error.name) {
                    error.name = 'UnknownError';
                }
                _self.onerror(error);
                _self.init();
                // rest of the code goes here
            };
        }
        this.rtcRecorder = null;
        this.rtcRecorder = recorder;
        return this;
    };
    RecorderUtill.prototype.addVideoStream = function (stream) {
        let _self = this;
        this.stream.getVideoTracks().forEach((t) => {
            _self.stream.removeTrack(t);
        });
        stream.getVideoTracks().forEach((t) => {
            console.log('!!!!![RecorderUtill] -- addVideoStream', t);

            _self.stream.addTrack(t);
        });
    };
    RecorderUtill.prototype.startRecording = function () {
        if(!this.rtcRecorder) console.warn('[recorderUtill] - recorder is not exist');
        try {
            this.rtcRecorder.startRecording();
        } catch (err) {
            console.log("inner startRecording ERROR: ", err)
        }
        return this;
    };
    RecorderUtill.prototype.stopRecording = function (callback = () =>{}) {
        if(!this.rtcRecorder) console.warn('[recorderUtill] - recorder is not exist');
        let _self = this;
        this.stream.getTracks().forEach((t) => {
            t.stop()
        });

        try {
            this.rtcRecorder.stopRecording(function() {
                // let blob = _self.rtcRecorder.getBlob();
                // _self.blobs.push(blob);
                _self.rtcRecorder.destroy();
                _self.audioDestination = null;
                callback();
            });
        } catch (err) {
            console.log("inner stopRecording ERROR: ", err)
        }
        return this;
    };
    RecorderUtill.prototype.appendAudioMixedStream = function(audioTracks) {

        if (!audioTracks) {
            throw 'First parameter is required.';
        }
        if (!(audioTracks instanceof Array)) {
            audioTracks = [audioTracks];
        }

        const at = new (window.AudioContext || window.webkitAudioContext)();
        let _self = this;
        _self.stream.getAudioTracks().forEach((t) => {
            _self.stream.removeTrack(t)
        });
        const sources = audioTracks.map(t => at.createMediaStreamSource(new MediaStream([t])));

// The destination will output one track of mixed audio.
        this.audioDestination = at.createMediaStreamDestination();
// Mixing
        sources.forEach(s => s.connect(this.audioDestination));
        this.stream.addTrack(this.audioDestination.stream.getTracks().filter(function(t) {
            return t.kind === 'audio';
        })[0]);
    };
    RecorderUtill.prototype.save = function (fileName, callback = () => {}) {
        if(this.blobs.length === 0) console.warn('[recorderUtill] - blobs.length ->', this.blobs.length)
        let _self = this;
        let buffers = [];
        let index = 0;
        _self.fileName = fileName;

        function readAsArrayBuffer() {
            if (!_self.blobs[index]) {
                return concatenateBuffers();
            }
            const reader = new FileReader();
            reader.onload = function(event) {
                buffers.push(event.target.result);
                index++;
                readAsArrayBuffer();
            };
            reader.readAsArrayBuffer(_self.blobs[index]);
        }

        readAsArrayBuffer();

        function concatenateBuffers() {
            let byteLength = 0;
            buffers.forEach(function(buffer) {
                byteLength += buffer.byteLength;
            });

            let tmp = new Uint16Array(byteLength);
            let lastOffset = 0;
            buffers.forEach(function(buffer) {
                // BYTES_PER_ELEMENT == 2 for Uint16Array
                let reusableByteLength = buffer.byteLength;
                if (reusableByteLength % 2 !== 0) {
                    buffer = buffer.slice(0, reusableByteLength - 1)
                }
                tmp.set(new Uint16Array(buffer), lastOffset);
                lastOffset += reusableByteLength;
            });

            let blob = new Blob([tmp.buffer], {
                type: _self.mimeType
            });
            let videoFile = new File([blob], fileName || 'RecordRTC', {
                type: _self.mimeType
            });

            let video = document.createElement('video');
            video.width = 256;
            video.height = 144;
            video.muted = true;
            video.preload = 'metadata';
            video.onloadeddata = function() {
                _self.createThumb(video, function (blob) {
                    let thumbnailFile = new File([blob], fileName + '.jpg' || 'RecordRTC', {
                        type: 'image/jpeg'
                    });
                    let sec = Math.floor((_self.duration % (1000 * 60)) / 1000),
                        result = { thumbnail: thumbnailFile, video: videoFile, duration: sec};
                    callback(result)
                });
                video.pause();
                video = null;
            };

            _self.videoFile = videoFile;
            video.src = URL.createObjectURL(videoFile);
            video.play()
        }
    };
    RecorderUtill.prototype.download = function () {

        let hyperlink = document.createElement('a');
        hyperlink.href = URL.createObjectURL(this.videoFile);
        hyperlink.download = this.fileName || 'RecordRTC';

        hyperlink.style = 'display:none;opacity:0;color:transparent;';
        (document.body || document.documentElement).appendChild(hyperlink);
        if (typeof hyperlink.click === 'function') {
            hyperlink.click();
        } else {
            hyperlink.target = '_blank';
            hyperlink.dispatchEvent(new MouseEvent('click', {
                view: window,
                bubbles: true,
                cancelable: true
            }));
        }
        URL.revokeObjectURL(hyperlink.href);
    };
    RecorderUtill.prototype.createThumb = function (video, callback = () => {}) {
        let c = document.createElement("canvas"),    // create a canvas
            ctx;                // get context
        c.width = video.width;   // set size = thumb
        c.height = video.height;
        ctx = c.getContext("2d");
        ctx.drawImage(video, 0, 0, c.width, c.height);
        let dataUrl = c.toDataURL("image/jpeg");
            let blob = dataURItoBlob(dataUrl);
        callback(blob);
        ctx = null;
        c = null;

        function dataURItoBlob(dataURI)
        {
            var byteString = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++)
            {
                ia[i] = byteString.charCodeAt(i);
            }

            var bb = new Blob([ab], { "type": mimeString });
            return bb;
        }
    };
    RecorderUtill.prototype.getVideoFile = function () {
        return this.file;
    };
    RecorderUtill.prototype.bytesToSize = function (bytes){
        let k = 1000;
        let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0) {
            return '0 Bytes';
        }
        let i = parseInt(Math.floor(Math.log(bytes) / Math.log(k)), 10);
        return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
    };
    return new RecorderUtill();
})();

export default recorderUtill
