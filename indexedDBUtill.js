
// (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)
if (!window.indexedDB) {
  window.alert("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.")
}

const indexedDBUtill = (() => {
  function indexedDBUtill () {
    this.db = null;
    this.store = null;
    this.dbName = 'emtyDataBase';
    this.storeName = 'emtyStore';
    this.transaction = null;

    this.open = function (option) {
      if (this.db) {
        return Promise.resolve();
      }
      if (!window.indexedDB) {
        console.log('browser does not support indexedDB');
      } else {
        this.dbName = option.dbName;
        this.storeName = option.storeName;

        let request = window.indexedDB.open(this.dbName);
        request.onerror = function (error) {
          console.log(error);
          return Promise.reject(this);
        }.bind(this);
        request.onupgradeneeded = function (event) {
          let db = event.target.result;
          let store = db.createObjectStore(option.storeName, {autoIncrement: true});

          option.indexList.forEach((index) => {
            store.createIndex(index, index , {unique: false});
          });

        }
        request.onsuccess = function (event) {
          this.db = event.target.result;
          return Promise.resolve();
        }.bind(this);
      }
    }
    this.add = function (data) {
      let transaction = this.db.transaction([this.storeName], 'readwrite');
      let store = transaction.objectStore(this.storeName);
      let request = store.add(data);
      let _self = this;
      return new Promise(function (resolve, reject) {
        request.onsuccess = function(e) {
          console.log('indexedDB added ---------- ', request.result);
          return resolve(request.result);
        };
        request.ontimeout = function (e) {
          console.log('timeout');
          _self.transaction.abort();
          return reject(this);
        }.bind(_self)
      });

    }
    this.deleteRange = function (lower, uppper) {
      let keyRangeValue = IDBKeyRange.bound(lower, upper, false, false);
      let transaction = this.db.transaction([this.storeName], 'readwrite');
      let store = transaction.objectStore(this.storeName);
      let request = store.delete(keyRangeValue);

      request.onsuccess = function(e) {
        console.log(request.result);
        return Promise.resolve(this);
      }.bind(this);

      request.ontimeout = function (e) {
        console.log('timeout');
        this.transaction.abort();
        return Promise.reject(this);
      }.bind(this)
    }
    this.deleteOnly = function (key) {
      let keyRangeValue = IDBKeyRange.only(key);
      let transaction = this.db.transaction([this.storeName], 'readwrite');
      let store = transaction.objectStore(this.storeName);
      let request = store.delete(keyRangeValue);

      request.onsuccess = function(e) {
        console.log(request.result);
        return Promise.resolve(this);
      }.bind(this);

      request.ontimeout = function (e) {
        console.log('timeout');
        this.transaction.abort();
        return Promise.reject(this);
      }.bind(this)
    }
    this.deleteLower = function (lower) {
      let keyRangeValue = IDBKeyRange.lowerBound(lower);
      let transaction = this.db.transaction([this.storeName], 'readwrite');
      let store = transaction.objectStore(this.storeName);
      let request = store.delete(keyRangeValue);

      request.onsuccess = function(e) {
        console.log(request.result);
        return Promise.resolve(request.result);
      }.bind(this);

      request.ontimeout = function (e) {
        console.log('timeout');
        this.transaction.abort();
        return Promise.reject(this);
      }.bind(this)
    }
    this.update = function (callObj, key) {
      let transaction = this.db.transaction([this.storeName], 'readwrite');
      let store = transaction.objectStore(this.storeName);
      let request = store.get(key);
      request.onsuccess = function(e) {
        var obj = e.target.result;

        store.put({...obj, ...callObj}, key);
        console.log(request.result);
        return Promise.resolve(this);
      }.bind(this);

      request.ontimeout = function (e) {
        console.log('timeout');
        this.transaction.abort();
        return Promise.reject(this);
      }.bind(this)
    },
    this.deleteUpper = function (upper) {
      let keyRangeValue = IDBKeyRange.upperBound(upper);
      let transaction = this.db.transaction([this.storeName], 'readwrite');
      let store = transaction.objectStore(this.storeName);
      let request = store.delete(keyRangeValue);

      request.onsuccess = function(e) {
        console.log(request.result);
        return Promise.resolve(this);
      }.bind(this);

      request.ontimeout = function (e) {
        console.log('timeout');
        this.transaction.abort();
        return Promise.reject(this);
      }.bind(this)
    }
    this.getAll = function (limit = 50) {
      let transaction = this.db.transaction([this.storeName], 'readwrite');
      let store = transaction.objectStore(this.storeName);
      let results  = [];
      let i = 0;

      store.openCursor(null, 'prev').onsuccess = function (event) {
        var cursor = event.target.result;
        if (cursor && i < limit) {
          results.push(cursor.value);
          i += 1;
          cursor.continue();
        }
        else {
          // Do something with results, which has at most 20 entries
          console.log(cursor);
        }
      };

      return results;
    }
  }
  return new indexedDBUtill();
})();

export default indexedDBUtill
