/**
 * Created by front on 2018-01-24.
 */
const errorMessages = {
    fieldIsRequired: '필드를 입력하세요.',
    emptyInput: '파라미터를 입력하세요.',
    invalidFile: '{0} 파일만 업로드 가능합니다. 최대 용량 100MB',
    invalidFile2: '(image 또는 ZIP)파일만 업로드 가능합니다. 최대 용량 100MB'
};
// const msg = (text, ...args) => { // arrow 함수에서는 arguments가 사라지고 args 가 생겻다.
//     if (text != null && args.length > 0) {
//         for (let i = 0; i < args.length; i++) {
//             text = text.replace('{' + i + '}', args[i]);
//         }
//     }
//     return text;
// };

const checkEmpty = (field) => {
	if (!field) return {
		valid: false,
		error: errorMessages.emptyInput
	}
    if (field.value === null || field.value === '') {
        if (field.required) {
            return {
                valid: false,
                error: errorMessages.fieldIsRequired
            }
        }
    }
    return {
        valid: true,
        error: ''
    }
};

const checkFile = (file) => {
	if (file) {
		const type = file.type;
		const size = file.size;
		if (!/(image\/gif)|(image\/png)|(image\/jpeg)|(application\/zip)|(application\/octet-stream)|(application\/x-zip-compressed)/gi.test(type) || size > (100 * 1024 * 1024)) {
			return {
				valid: false,
				error: '(image 또는 ZIP)파일만 업로드 가능합니다. 최대 용량 100MB'
			}
		}
	} else {
		return {
			valid: false,
			error:errorMessages.emptyInput
		};
	}

	return {
		valid: true,
		error: ''
	};
};

const checkField = (field) => {
    let validation;
    validation = checkEmpty(field);
    if (validation.valid && typeof field.validatorFunc === 'function') {
        Object.assign(validation, field.validatorFunc(field));
    }
    return validation;
};

const checkAllField = (scheme) => {
	let err = {};
	let obj = {};
	if (scheme) {
		Object.keys(scheme).forEach((key) => {
			let field = scheme[key];
			let validation = checkField(field);
			err[key] = validation;
			if (!err[key].valid) {
				Object.assign(obj, err)
			}
		});
		return obj;
	}
	return {};
};

const validator = {
	errorMessages,
    required (field) {
	    return checkEmpty(field);
    },
	checkEmpty,
    checkField,
    checkFile,
    checkAllField
};

export default validator;
