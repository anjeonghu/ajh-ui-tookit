/**
 * Created by front on 2018-03-05.
 */


define(['exports','jquery'], function (exports, $) {
    'use strict';

    // 자동 스크롤 기능
    /*
     * 사용법
     * init 함수 호출
     * var scroll ScrollSync.init('#section-auto-scroll');
     * scroll.top - 해당 element의 오프셋 탑 으로 이동
     * scroll.next - 해당 element의 bottom으로 이동
     * scroll.before - 이동한 해당 element의 이전 element 로 이동
     */
    var ScrollSync = (function () {
        var scrollTarget,
            scrollTargetHeight,
            targetBottom,
            targetTop,
            timeout,
            iScrollTop,
            iCurScrollTop,
            isDiabled;

        function init(target) {
            if (target === undefined) return;

            scrollTarget = $(target);
            scrollTargetHeight = scrollTarget.height();
            targetTop = scrollTarget.offset().top;
            targetBottom = targetTop + scrollTargetHeight;
            iScrollTop = $(window).scrollTop();
            isDiabled = false;

            $(window).resize(resizeCallback);
            $(window).on('scroll', onScrollCallback);
        }


        function resizeCallback() {
            scrollTarget = $(scrollTarget);
            scrollTargetHeight = scrollTarget.height();
            targetTop = scrollTarget.offset().top;
            targetBottom = targetTop + scrollTargetHeight;
            iScrollTop = $(window).scrollTop();
        }
        function moveScrollTop() {
            animation(targetTop);
        }
        function moveScrollBefore() {
            var top = targetTop - $(window).height(); // 현재 보고있는 브라우저 높이 만큼 뒤로 가야 한다.
            animation(top);
        }
        function moveScrollNext() {
            animation(targetBottom);
        }

        function animation(top) {
            $(window).off('scroll');
            if (isDiabled === false) {
                isDiabled = true;
                $('body, html').animate({ 'scrollTop': top }, 500, 'swing', function () {

                    clearTimeout(timeout);
                    iScrollTop = $(window).scrollTop();

                    timeout = setTimeout(function () {
                        $(window).on('scroll', onScrollCallback);
                        isDiabled = false;
                    }, 100);
                });
            }
        }
        function onScrollCallback() {
            iCurScrollTop = $(window).scrollTop();

            if (iCurScrollTop > iScrollTop) {  // scroll down
                var startY = targetTop - 500;
                var endY = targetTop;

                if (iCurScrollTop > startY && iCurScrollTop < endY) {
                    moveScrollTop();
                }
                else {
                    iScrollTop = $(window).scrollTop();
                }
            }
            else if (iCurScrollTop < iScrollTop) { // scroll up
                scrollTargetHeight = scrollTarget.height();
                var startY = (targetTop + scrollTargetHeight) - 500;
                var endY = (targetTop + scrollTargetHeight);

                if (iCurScrollTop > startY && iCurScrollTop < endY) {
                    moveScrollTop();
                }
                else {
                    iScrollTop = $(window).scrollTop();
                }
            }
            else {
                iScrollTop = $(window).scrollTop();
            }
        }
        return {
            scrollTarget: scrollTarget,
            scrollTargetHeight: scrollTargetHeight,
            targetTop: targetTop,
            targetBottom: targetBottom,
            init: init,
            top: moveScrollTop,
            before: moveScrollBefore,
            next: moveScrollNext
        }
    })()

    return ScrollSync;

});
////# sourceMappingURL=common.js.map