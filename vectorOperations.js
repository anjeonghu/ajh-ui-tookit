﻿

// radian 각도 degree 각도로
function getDegree (radian) {
    return radian / (Math.PI / 180);
}

// degree 각도 radian 각도로
function getRadian (deg) {
    return (Math.PI / 180) * deg;
}

// 벡터의 크기 계산

function getMagnitude(vec) {
    return Math.sqrt(Math.pow(vec.x,2)+Math.pow(vec.y,2)+Math.pow(vec.z,2))
}

// 벡터의 정규화
function normalizeVector(vec){
    var mag = getMagnitude(vec);
    return { x:vec.x / mag, y:vec.y / mag, z:vec.z / mag}
}

// 두벡터의 내적
function getDot3Dvector (vecA, vecB){
    return (vecA.x * vecB.x) + (vecA.y*vecB.y) + (vecA.z*vecB.z)
}

// 두벡터 사이의 각
function getDegBetweenVectors (vecA, vecB) {
    // 공식
    // A 와 B의 내적 은 ||A||B||cos라디언
    var magA = getMagnitude(vecA);
    var magB = getMagnitude(vecB);
    var dot = getDot3Dvector(vecA, vecB);
    var radian = Math.acos(dot / (magA * magB));
    return getDegree(radian);
}


console.log(getDegBetweenVectors({x:5,y:2,z:-3}, {x:8,y:1,z:-4}));