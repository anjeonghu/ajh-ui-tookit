/**
 * Created by front on 2018-01-24.
 */

/*
*
* 테스트 하기 좋은 코드 -> 잘 설계된 코드 -> 소프트 공학원칙을 잘 따른 코드
1. SRP(단일책임의 원칙:Single Responsibility Principle)

   - 작성된 클래스는 독립된 기능을 수행해야하고 하나의 책임을 가진다.
   -  관심사를 분리하자.
2.OCP(개방폐쇄의 원칙: Open Close Princle)
  - 클래스 또는 모듈은 확장에 열려있고, 변경에는 닫혀 있어야 한다.
  - 실행코드는 변경하지 말고 어떻게든 (상속등의 방법)으로 재사용하고 확장한다.
3.LSP (리스코브 치환의 원칙: The Liskov Substitution Principle)
  - 어떤 타입에서 파생된 타입의 객체가 있다면 이 타입을 사용하는 코드는 변경하지 말아야 한다.
4.ISP (인터페이스 분리의 원칙: Interface Segregation Principle)


* */


import validator from "./validationForm";

describe('checkEmpty', () => {
	let field = null;
	beforeEach(() => {
		field = {
			value: 'happenask@naver.com',
			required: true,
		}
	})

	// 실패 사례
	it('field 없을때 valid false 할당하고 error에 에러메세지를 할당한다.', () => {
		const res = validator.checkEmpty();
		expect(res).toEqual({
			valid: false,
			error:validator.errorMessages.emptyInput
		})
	});

	it('filed.value가 null 이거나 빈값일때 field.required가 true이면 valid에 false를 할당하고 error 속성에 에러메세지를 할당한다.', () => {
		field = {
			value: '',
			required: true,
		}
		const res = validator.checkEmpty(field);
		expect(res).toEqual({
			valid: false,
			error:validator.errorMessages.fieldIsRequired
		})
	});

	// 성공 사례
	it('field 파라미터가 올바를때 valid에 true를 할당한다.', () => {
		const res = validator.checkEmpty(field);
		expect(res).toEqual({
			valid: true,
			error:''
		})
	});
})

// const checkField = (field) => {
// 	let validation;
// 	validation = checkEmpty(field);
// 	if (validation.valid && typeof field.validator === 'function') {
// 		Object.assign(validation, field.validator(field));
// 	}
// 	return validation;
// };



describe('checkField', () => {
	let feild = null;
	let regExp = null;

	beforeEach(() => {
		regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
		feild = {
			value: 'happenask@naver.com',
			required: true,
			validatorFunc: (value) => {
				let valid = regExp.test(value)
				return {valid, error: ''}; // 형식에 맞는 경우 true 리턴
			}
		}

		spyOn(feild, 'validatorFunc');
	})

	// 실패 사례
	it('checkEmpty(filed)의 return object인 validation 의 valid false 이면 validation을 바로 리턴한다.', () => {
		feild = {
			value: '',
			required: true,
			error: ''
		}
		const res = validator.checkEmpty(feild);
		expect(res).toEqual({
			valid: false,
			error:validator.errorMessages.fieldIsRequired
		})
	});

	// 성공 사례
	it('checkEmpty(filed)의 return object인 validation 의 valid true 이고 typeof field.validator === function 이면 validator를 실행한다.', () => {
		const res = validator.checkField(feild);
		expect(feild.validatorFunc).toHaveBeenCalled();
	});

	it('field.validator 함수가 있고 validator함수를 통과 하면 valid true, error에 빈값을 할당한다..', () => {
		const res = validator.checkField(feild);
		expect(feild.validatorFunc).toHaveBeenCalled();
		expect(res).toEqual({valid: true, error: ''})
	});
})


describe('checkFile', () => {
	let file = null;

	// 실패 사례
	it('file 인자 가 없으면 valid에 false를 할당하고 error에 에러메세지를 할당한다.', () => {
		const res = validator.checkFile();
		expect(res).toEqual({
			valid: false,
			error: validator.errorMessages.emptyInput
		})
	});

	it('file 지원 형식에 맞지 않으면 valid에 false error에 에러메세지를 할당한다.', () => {
		file = {};
		const res = validator.checkFile(file);
		expect(res).toEqual({
			valid: false,
			error:validator.errorMessages.invalidFile2
		})
	});

	// 성공 사례
	it('file이 지원하는 파일 형식에 맞으면 valid에 true, error에 빈값을 할당한다.', () => {
		file = new Image();
		const res = validator.checkField(file);
		expect(res).toEqual({
			valid: true,
			error:''
		})
	});
})


describe('checkAllField', () => {
	let scheme = null;

	beforeEach(() => {
		scheme = {
			userName: {
				value: "",
				required: true,
			},
			password: {
				value: "",
				required: true,
			},
		}
	})

	// 실패 사례
	it('scheme 인자 가 없으면 빈 객체를 반환한다.', () => {
		const res = validator.checkAllField();
		expect(Object.keys(res).length).toBe(0);
	});

	// 성공 사례
	describe('scheme 에 정의된 모든 property validation check 하고 객체를 반환한다. ', () => {
		it('scheme 중 모두 valid false 인경우 ', () => {
			let expectObj = {
				userName: {valid: false, error: validator.errorMessages.fieldIsRequired},
				password: {valid: false, error: validator.errorMessages.fieldIsRequired},
			}
			const res = validator.checkAllField(scheme);
			expect(res).toEqual(expectObj)
		})

		it('scheme 중 모두 valid true 인경우 ', () => {
			scheme = {
				userName: {
					value: "happenask@naver.com",
					required: true,
				},
				password: {
					value: "minttest",
					required: true,
				},
			}
			const res = validator.checkAllField(scheme);
			expect(Object.keys(res).length).toBe(0);
		})

		it('scheme 중 모두 일부만 valid true 인경우 ', () => {
			scheme = {
				userName: {
					value: "happenask@naver.com",
					required: true,
				},
				password: {
					value: "",
					required: true,
				},
			}
			let expectObj = {
				password: {
					error: validator.errorMessages.fieldIsRequired,
					valid: false
				},
				userName: {
					error: "",
					valid: true
				}
			}
			const res = validator.checkAllField(scheme);
			expect(res).toEqual(expectObj)
		})
	});
})