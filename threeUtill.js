/**
 * Created by front on 2017-06-09.
 */
import * as THREE from 'three';


const ThreeUtill = function () {
  this.width = 1000;
  this.height = 1000;
  this.realWidth = 1280;
  this.realHeight = 720;

  this.DEFAULT_CAMERA = new THREE.PerspectiveCamera(
    60,
    this.width / this.height,
    0.3,
    5000
  );
  this.DEFAULT_CAMERA.name = 'PerspectiveCamera';
  //this.DEFAULT_CAMERA.position.set(0,0, 0);
  this.DEFAULT_CAMERA.position.set(0, 0, 0);
  this.DEFAULT_CAMERA.lookAt( new THREE.Vector3(0, 0, -1) );

  //this.light = new THREE.DirectionalLight(0xffffff, 0.8);
  this.scenes = {};
  this.scene = new THREE.Scene();
  this.scene.name = 'Scene #1';
  this.scenes[this.scene.uuid] = this.scene;
  this.renderer = new THREE.WebGLRenderer( { alpha: true} );
  this.renderer.setClearColor(new THREE.Color(0x000000), 0);
  this.camera = this.DEFAULT_CAMERA.clone();
  this.materialsMap = {};
  this.meshesMap = {};
  this.index = 0;

};


ThreeUtill.prototype =  {
  setProjectioMatrix: function (width, height) {
    let near = 0.3;
    let far = 5000.0;
    let cw = this.realWidth;
    let ch = this.realHeight;
    let vw = width;
    let vh = height;
    let x = 0;
    let y = 0;

    let w = cw;
    let h = 0;
    let sr = 0;
    let cr = cw / ch;
    h = cw * vh / vw;
    sr = vw / vh;


    if (sr < cr) {
      x = h / w * cr;
      y = cr;
    } else {
      x = 1;
      y = w/h;
    }

    let result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    result[0] = 2 * x;
    result[5] = 2 * -y;

    result[10] = (far + near) / (far - near);
    result[11] = 1;
    result[14] = -(2.0 * far * near) / (far - near);

    console.log('projection matrix --', result);

    return result;
  },
  setScene: function (scene) {
    if(scene !== null && this.scene !== scene ) {
      if (scene.name === '') scene.name = `Scene #${Object.keys(this.scenes).length + 1}`;
      this.scene = scene;
      this.scenes[scene.uuid] = this.scenes[scene.uuid] || scene;
    }

    return this;
  },
  setCamara: function (camera){
    this.camera = camera;
    if ( this.camera && this.camera instanceof THREE.PerspectiveCamera) {

      this.camera.aspect = this.width / this.height;

    } else if (this.camera instanceof THREE.OrthographicCamera) {
      this.camera.left = -this.width / 2000;
      this.camera.right = this.width / 2000;
      this.camera.top = this.height / 2000;
      this.camera.bottom = -this.height / 2000;
    }
    this.camera.updateProjectionMatrix();

    return this;
  },
  setSize: function (width, height) {
    this.width = width;
    this.height = height;

    if ( this.camera && this.camera instanceof THREE.PerspectiveCamera) {

      this.camera.aspect = this.width / this.height;
      console.log("camera width", this.width);
      console.log("camera height", this.height);

      let projectionMatrix = this.setProjectioMatrix(width, height);
      this.camera.projectionMatrix.elements = projectionMatrix;
      //this.camera.updateProjectionMatrix();
    }

    if ( this.renderer ) {

      this.renderer.setSize( width, height );

    }
    this.render();

    return this;
  },
  addTrackingMesh: function ( id, texture, color, anchorPose = null ) {
    console.log('this.camera.projectionMatrix --- ', this.camera.projectionMatrix)
    let _self = this;
    let mesh = null;

    let loader = new THREE.TextureLoader();

    return new Promise(function (resolve, reject) {
      if (typeof texture === "string") {
        loader.load(
          texture,
          function (imageTexture) {
            imageTexture.flipY = false;
            imageTexture.needsUpdate = true;
            imageTexture.magFilter = THREE.LinearFilter;
            imageTexture.minFilter = THREE.LinearFilter;

            let material = new THREE.MeshBasicMaterial({side: THREE.DoubleSide, depthTest: false, depthWrite: false});
            material.transparent = true;
            material.map = imageTexture;
            material.color = new THREE.Color(`rgb(${color.r}, ${color.g}, ${color.b})`);

            mesh = new THREE.Mesh(new THREE.PlaneGeometry(imageTexture.image.width, imageTexture.image.height, 2, 2), material);
            mesh.castShadow = true;
            mesh.receiveShadow = false;
            mesh.userData.anchorPose = anchorPose;
            mesh.matrixAutoUpdate = false;

            _self.scene.add( mesh );

            //_self.render();
            if (Object.prototype.hasOwnProperty.call(_self.meshesMap, id)){
              _self.materialsMap[id].push(mesh.material);
              _self.meshesMap[id].push(mesh);
            } else {
              _self.materialsMap[id] = [mesh.material];
              _self.meshesMap[id] = [mesh];
            }
            resolve(mesh.id);
          },
          undefined,
          function (err) {
            console.error('threeUtill has An error happened.', err);
          }
        );
      }
      else if (texture && texture.isTexture) {
        texture.needsUpdate = true;
        //texture.magFilter = THREE.NearestFilter;
        texture.generateMipmaps = false;
        texture.magFilter = THREE.LinearFilter;
        texture.minFilter = THREE.LinearFilter;

        let material = new THREE.MeshBasicMaterial({side: THREE.DoubleSide, depthTest: false, depthWrite: false});
        material.transparent = true;
        material.map = texture;
        material.color = new THREE.Color(`rgb(${color.r}, ${color.g}, ${color.b})`);

        mesh = new THREE.Mesh(new THREE.PlaneGeometry(texture.image.width, texture.image.height, 2, 2), material);

        mesh.castShadow = true;
        mesh.receiveShadow = false;

        mesh.userData.anchorPose = anchorPose;
        mesh.matrixAutoUpdate = false;

        _self.scene.add( mesh );

        if (Object.prototype.hasOwnProperty.call(_self.meshesMap, id)){
          _self.materialsMap[id].push(mesh.material);
          _self.meshesMap[id].push(mesh);
        } else {
          _self.materialsMap[id] = [mesh.material];
          _self.meshesMap[id] = [mesh];
        }

        resolve(mesh.id);
      } else {
        reject();
        console.log('올바른 Texture 가 아닙니다............................................')
      }
      //todo
      // resolve(mesh)


      console.log('addTrackingMesh')
    });

  },
  setMaterialsColor: function (id, color) {
    if (this.materialsMap[id] !== undefined) {
      this.materialsMap[id].forEach((material) => {
        material.color = new THREE.Color(`rgb(${color.r}, ${color.g}, ${color.b})`);
      });
    }

    this.render();
  },
  getTrackingObjects: function () {
    return this.scene.children;
  },
  updateMeshById: function (id, anchorPose) {
    let mesh = this.scene.getObjectById(id);
    if (mesh !== undefined) {
      mesh.userData.anchorPose = anchorPose
    }

  },
  updateAllMeshes: function (trackingPose){

    let _self = this;

    let meshes = this.scene.children;

    if (meshes.length > 0) {
      meshes.forEach((mesh) => {
        if (mesh.userData.anchorPose !== null && mesh.userData.anchorPose !== undefined) {
          let localScaleMatrix = new THREE.Matrix4();
          localScaleMatrix.identity();
          localScaleMatrix.makeScale(1 / _self.width, 1 / _self.width, 1);

          let modelMatrix = new THREE.Matrix4();
          let anchorMatrix = new THREE.Matrix4();
          let trackingMatrix = new THREE.Matrix4();

          modelMatrix.identity();

          anchorMatrix.elements = mesh.userData.anchorPose;
          trackingMatrix.elements =  trackingPose;
          //trackingMatrix.elements[13] = -trackingMatrix.elements[13];

          modelMatrix.multiplyMatrices(modelMatrix, localScaleMatrix);
          modelMatrix.multiplyMatrices(anchorMatrix, modelMatrix);
          modelMatrix.multiplyMatrices(trackingMatrix, modelMatrix);

          mesh.matrix = modelMatrix;
          mesh.__dirtyPosition = true;
          mesh.__dirtyRotation = true;
          mesh.updateMatrixWorld();
        }

        // _self.render();
      });
      this.render();
    }

  },
  removeObject: function ( object ) {
    if (object.parent === null) return;

    object.parent.remove( object );
    return this;
  },

  imageCapture: function () {
    let mime = 'image/jpeg';
    return this.renderer.domElement.toDataURL(mime);
  },
  clear: function (id) {
    if(id && this.meshesMap[id]) {
      while(this.meshesMap[id].length > 0){
        this.scene.remove(this.meshesMap[id].pop());
      }
    }
    this.render();
    return this;
  },
  clearAll: function () {
    this.materialsMap = {};
    this.meshesMap = {};
    while(this.scene.children.length > 0){
      this.scene.remove(this.scene.children[0]);
    }
    this.render();
    return this;
  },
  undo: function (id) {
  //  this.scene.remove(this.meshMap[id].pop());
    this.scene.remove(this.meshesMap[id].pop());

    this.render();
    return this.meshesMap[id].length;
  },
  visible: function (visible) {
    let meshes = this.scene.children;

    if (meshes.length > 0) {
      meshes.forEach((mesh) => {
        mesh.visible = visible;
      })
    }
    this.render();
  },
  render: function () {
    this.renderer.render(this.scene, this.camera);
  }

};

export default new ThreeUtill ();
