/**
 * Created by front on 2017-06-09.
 */

const canvasConfig = ()=>{

  let cx = 0;
  let cy = 0;
  let sx = 0;
  let sy = 0;
  let sw = 0;
  let sh = 0;

  // let sRatio=0;
  // let rsx =0;
  // let rsy =0;
  let realWidth = 1;
  let realHeight = 1;

  const getRealCoordinate = (obj, width = 1, height = 1, offsetWidth = 1, offsetHeight = 1) => {
    let x=0,y=0;
    x = Number((obj.x * (width / offsetWidth)).toFixed(2));
    y = Number((obj.y * (height / offsetHeight)).toFixed(2));

    return {x, y}
  };
  const getRealRatio = (width = 1, offsetWidth = 1) => {
    let ratio = (width / offsetWidth).toFixed(2);

    return Number(ratio)
  };

  const imageDrawingCover = (ctx, img, x, y, width, height, pos, opts) =>{
    opts = Object.assign({ cx: cx, cy: cy, zoom: 1, alpha: 1 }, opts || {});

    cx = parseInt(pos[0])/100;
    cy = parseInt(pos[1])/100;

    if (cx < 0 || cx > 1) throw new Error('Make sure 0 < opt.cx < 1 ');
    if (cy < 0 || cy > 1) throw new Error('Make sure 0 < opt.cy < 1 ');
    if (opts.zoom < 1) throw new Error('opts.zoom not >= 1');

    const ir = img.width / img.height;
    const r = width / height;

    sw = (ir < r ? img.width : img.height * r) / opts.zoom;
    sh = (ir < r ? img.width / r : img.height) / opts.zoom;
    sx = (img.width - sw) * cx;
    sy = (img.height - sh) * cy;
    // sRatio  = width/sw;
    // rsx = sx * sRatio;
    // rsy = sy * sRatio;

    ctx.save();
    ctx.globalAlpha = opts.alpha;
    ctx.drawImage(img, sx, sy, sw, sh, x, y, width, height);
    ctx.restore();
  };

  const drawLineByPoints = (ctx, stroke, color = 'rgba(255, 255, 255, 1)') => {

    if(stroke === undefined) return new Error('[drawLineByPoints] --- stroke is undefined');

    let line = stroke.line;
    let offsetWidth = realWidth;
    let offsetHeight = realHeight;
    let fontSize = stroke.width;

    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.lineWidth = fontSize;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.shadowColor = color //`rgb(${color.r}, ${color.g}, ${color.b})`;

    let x = line[0].x;
    let y = line[0].y;

    let point = getRealCoordinate({x, y}, ctx.canvas.width, ctx.canvas.height, offsetWidth, offsetHeight);

    let left = point.x,
      right = point.x,
      top = point.y,
      bottom = point.y;

    ctx.moveTo(point.x, point.y);

    for (let i = 1; i < line.length; i ++) {
      let x = line[i].x;
      let y = line[i].y;

      point = getRealCoordinate({x, y}, ctx.canvas.width, ctx.canvas.height, offsetWidth, offsetHeight);

      left = Math.min(point.x, left);
      right = Math.max(point.x, right);
      top = Math.min(point.y, top);
      bottom = Math.max(point.y, bottom);

      ctx.lineTo(point.x, point.y);
    }

    ctx.stroke();

    left = Math.max(left - 5, 0);
    right = Math.min(right + 5, ctx.canvas.width);
    top = Math.max(top - 5, 0);
    bottom = Math.min(bottom + 5, ctx.canvas.height);

    let width = right - left;
    let height = bottom - top;

    let imgData = ctx.getImageData(left, top, width, height);
    let boundingBox = {left, right, top, bottom, width, height, imgData };

    return {ctx, boundingBox}
  };

  const getLabelImage = function (canvas, text) {
    var ctx = canvas.getContext("2d");
    ctx.moveTo(0,0);
    ctx.fillStyle = 'rgba(255, 255, 255)';
    ctx.fillRect(1,1, canvas.width - 2, 40); // 1px margin 값을 주기 위해
    ctx.fillStyle = "#000000";
    ctx.font = "20px OpenSans";
    ctx.textBaseline = 'middle';
    ctx.textAlign = "center";
    ctx.fillText(text, canvas.width / 2, canvas.height / 2);
    ctx.beginPath();
    ctx.moveTo(canvas.width / 2, 41);
    ctx.lineTo(canvas.width / 2, 60);
    ctx.strokeStyle = 'rgba(255, 255, 255)';
    ctx.stroke();
    ctx.closePath();
    ctx.beginPath();
    ctx.arc(canvas.width / 2, 56, 5, 0, Math.PI * 2, true);
    ctx.fillStyle = 'rgba(255, 255, 255)';
    ctx.fill();
    ctx.closePath();

    let imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);

    return imgData
  }

  const imageCapture = (canvas, video) => {
    let context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(video, 0, 0, canvas.width,
      canvas.height);
  };

  const setSize = (w, h) => {
    realWidth = w;
    realHeight = h;
  }

  return {
    getRealCoordinate:getRealCoordinate,
    getRealRatio:getRealRatio,
    drawLineByPoints:drawLineByPoints,
    imageDrawingCover:imageDrawingCover,
    imageCapture:imageCapture,
    setSize: setSize,
    getLabelImage: getLabelImage
  }

};
const canvasUtil = canvasConfig();
export default canvasUtil;
